<?php /** @noinspection PhpUnused */
declare(strict_types=1);

namespace Was\TestsRecettes\Controllers;

use Template;

/**
 * Class Index
 *
 * @package Was\TestRecettes\Controllers
 */
class Index
{
    /**
     * Effectue des opérations avant l'éxécution de la route
     */
    public function beforeroute(): void
    {
    }

    /**
     * Méthode par défaut du controlleur par défaut
     */
    public function index(): void
    {
        echo Template::instance()->render('index.html');
    }

    /**
     * Effectue des opérations après l'éxécution de la route
     */
    public function afterroute(): void
    {
    }
}