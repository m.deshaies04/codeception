<?php declare(strict_types=1);

namespace Was\TestsRecettes\Response;

/**
 * Interface ErrorManagerInterface
 *
 * @package Was\TestsRecettes\Security
 */
interface ErrorManagerInterface
{
    /**
     * Ajoute une nouvelle erreur à l'errorManager
     *
     * @param int $errorCode
     * @return void
     */
    public function addError(int $errorCode): void;

    /**
     * Transforme un code en message d'erreur
     *
     * @param int $errorCode
     * @return array
     */
    public static function errorCodeToMessage(int $errorCode): array;

    /**
     * Vérifie si le errorCode contient une erreur
     *
     * @param int $errorCode
     * @return bool
     */
    public static function hasError(int $errorCode): bool;
}