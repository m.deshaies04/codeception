<?php declare(strict_types=1);

namespace Was\TestsRecettes\Helper;

/**
 * Interface StrInterface
 *
 * @package Was\TestsRecettes\Helper
 */
interface StrInterface
{
    /**
     * Vérifie si le texte donnée est un palindrome
     *
     * @param string $text
     * @return bool
     */
    public static function isPalindrome(string $text): bool;

    /**
     * Compare deux textes afin de vérifier si ce sont des anagrammes
     *
     * @param string $first
     * @param string $second
     * @return bool
     */
    public static function isAnagram(string $first, string $second): bool;

    /**
     * Incrémente un texte avec un délimiteur
     *
     * @param string $string
     * @param string $delimiter
     * @return string
     */
    public static function increment(string $string, string $delimiter = '_'): string;

    /**
     * Décrémente un texte avec un délimiteur
     *
     * @param string $string
     * @param string $delimiter
     * @return string
     */
    public static function decrement(string $string, string $delimiter = '_'): string;

    /**
     * Retourne le nombre de voyelles dans un texte donné
     *
     * @param string $text
     * @return int
     */
    public static function countVowels(string $text): int;

    /**
     * Vérifie si un texte donné est en minuscules
     *
     * @param string $text
     * @return bool
     */
    public static function isLower(string $text): bool;

    /**
     * Vérifie si un texte donné est en majuscule
     *
     * @param string $text
     * @return bool
     */
    public static function isUpper(string $text): bool;
}