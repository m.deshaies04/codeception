<?php /** @noinspection PhpUnused */
/** @noinspection ReturnTypeCanBeDeclaredInspection */
declare(strict_types=1);

namespace Was\TestsRecettes\Helper;

use ArrayAccess;

/**
 * Class Collection
 *
 * @package Was\TestsRecettes\Helper
 */
class Collection implements ArrayAccess, CollectionInterface
{
    /**
     * Collection constructor.
     *
     * @param array $arr
     */
    public function __construct(array $arr = [])
    {
        foreach ($arr as $key => $value) {
            if (is_array($value)) {
                $this->$key = new self($value);
            } else {
                $this->$key = $value;
            }
        }
    }

    /**
     * Filtre les résultats d'une collection
     *
     * @param callable|null $callback
     * @return array
     */
    public function filter(?callable $callback = null): array
    {
        return Arr::filter($this->toArray(), $callback);
    }

    /**
     * Vérifie si l'index existe dans la collection
     *
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        return isset($this->$offset);
    }

    /**
     * Retourne la valeur d'un index donné
     *
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        // TODO Un retour inatendu est une erreur, retournez plutôt une RuntimeException

        return $this->$offset ?? false;
    }

    /**
     * Définit une valeur à un index donné
     *
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    /**
     * Supprime la valeur d'un index donné
     *
     * @param mixed $offset
     */
    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }

    /**
     * Convertit la collection en tableau
     *
     * @return array
     */
    public function toArray(): array
    {
        return get_object_vars($this);
    }
}